﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyWay_Quotation.Models;

namespace MyWay_Quotation.ViewModels
{
    public class IndexViewModel
    {
        public string CompanyAddr { set; get; }
        public string CompanyPhone { set; get; }
        public string CompanyUninum { set; get; }
        public string CompanyEmail { set; get; }
        public string CompanyName { set; get; }

        public List<MywayProduct> MywayProds { set; get; }

        public List<string> CostumNames { set; get; }
        public Cusorder Cusorder { set; get; }
        public User_remarks Remarks { set; get; }
        public Cuscompany Cuscompany { set; get; }

        public List<CustomInfo> CostumList {set;get;}
    }

    public class CustomInfo
    { 
        public string label { set; get; }
        public string value { set; get; }

        public CusCompany company { set; get; }
        public CusOrder order { set; get; }
    }

    //public class CustomInfo
    //{
    //    public string label { set; get; }
    //    public string value { set; get; }

    //    public string Cusid { get; set; }
    //    public string Name { get; set; }
    //    public string CoName { get; set; }
    //    public string CoAddress { get; set; }
    //    public string CoPhone { get; set; }

    //    public string uniNum { set; get; }
    //    public DateTime? Createdate { get; set; }
    //    public DateTime? Updatedate { get; set; }
    //}

    public class Cuscompany
    {
        public string Cusid { get; set; }
        public string Name { get; set; }
        public string CoName { get; set; }
        public string CoAddress { get; set; }
        public string CoPhone { get; set; }
        public DateTime Createdate { get; set; }
        public DateTime Updatedate { get; set; }
    }
    public class Cusorder
    {
        public string Uninum { set; get; }
        public DateTime? Trandate { set; get; }
        public string Quodate { set; get; }
        public DateTime? Valdate { set; get; }

    }
    public class User_remarks
    {
        public string Warranty { set; get; }
        public string Origin { set; get; }
        public string Delivery_date { set; get; }
        public string Holding { set; get; }
        public string Service_date { set; get; }
    
    }
    public class MywayProduct
    { 
        public string Name { set; get; }
        public string Value { set; get; }
    }

}
