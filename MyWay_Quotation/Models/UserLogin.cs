﻿using System;
using System.Collections.Generic;

namespace MyWay_Quotation.Models
{
    public partial class UserLogin
    {
        public string Userid { get; set; }
        public string Pw { get; set; }
        public string Ac { get; set; }
        public DateTime Createdate { get; set; }
        public DateTime Updatedate { get; set; }
        public string Flag { get; set; }
    }
}
