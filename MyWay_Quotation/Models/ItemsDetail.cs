﻿using System;
using System.Collections.Generic;

namespace MyWay_Quotation.Models
{
    public partial class ItemsDetail
    {
        public string Items { get; set; }
        public string Numid { get; set; }
        public string Spec { get; set; }
        public int Quan { get; set; }
        public int Price { get; set; }
        public int Discount { get; set; }
    }
}
