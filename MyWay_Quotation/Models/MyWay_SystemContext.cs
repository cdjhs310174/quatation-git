﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MyWay_Quotation.Models
{
    public partial class MyWay_SystemContext : DbContext
    {
        public MyWay_SystemContext()
        {
        }

        public MyWay_SystemContext(DbContextOptions<MyWay_SystemContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CusCompany> CusCompany { get; set; }
        public virtual DbSet<CusOrder> CusOrder { get; set; }
        public virtual DbSet<ItemsDetail> ItemsDetail { get; set; }
        public virtual DbSet<OrderItems> OrderItems { get; set; }
        public virtual DbSet<UserInfo> UserInfo { get; set; }
        public virtual DbSet<UserLogin> UserLogin { get; set; }
        public virtual DbSet<UserRemarks> UserRemarks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("data source=127.0.0.1,1433;Initial Catalog=MyWay_System;User ID=sa;Password=871227;Database=MyWay_System;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CusCompany>(entity =>
            {
                entity.HasKey(e => e.Cusid)
                    .HasName("PK_Cus_company_1");

                entity.ToTable("Cus_company");

                entity.Property(e => e.Cusid).ValueGeneratedNever();

                entity.Property(e => e.CoAddress)
                    .HasColumnName("Co_address")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CoName)
                    .HasColumnName("Co_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CoPhone)
                    .HasColumnName("Co_phone")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate).HasColumnType("date");

                entity.Property(e => e.Name)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Unique).ValueGeneratedOnAdd();

                entity.Property(e => e.Updatedate).HasColumnType("date");
            });

            modelBuilder.Entity<CusOrder>(entity =>
            {
                entity.HasKey(e => e.Unique);

                entity.ToTable("Cus_order");

                entity.Property(e => e.Quodate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Trandate).HasColumnType("date");

                entity.Property(e => e.Valdate).HasColumnType("date");
            });

            modelBuilder.Entity<ItemsDetail>(entity =>
            {
                entity.HasKey(e => e.Items);

                entity.ToTable("Items_detail");

                entity.Property(e => e.Items)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Numid)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Spec)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrderItems>(entity =>
            {
                entity.HasKey(e => e.Cusid);

                entity.ToTable("Order_items");

                entity.Property(e => e.Cusid)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate).HasColumnType("date");

                entity.Property(e => e.Itemsid)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Updatedate)
                    .HasColumnName("updatedate")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<UserInfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("User_info");

                entity.Property(e => e.Addr)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Uninum)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Userid)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserLogin>(entity =>
            {
                entity.HasKey(e => e.Userid)
                    .HasName("PK_User_login_1");

                entity.ToTable("User_login");

                entity.Property(e => e.Userid)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Ac)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Createdate).HasColumnType("date");

                entity.Property(e => e.Flag)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Pw)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Updatedate).HasColumnType("date");
            });

            modelBuilder.Entity<UserRemarks>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("User_remarks");

                entity.Property(e => e.Cusid)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DeliveryDate)
                    .IsRequired()
                    .HasColumnName("Delivery_date")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Holding)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Origin)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceDate)
                    .IsRequired()
                    .HasColumnName("Service_date")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Warranty)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
