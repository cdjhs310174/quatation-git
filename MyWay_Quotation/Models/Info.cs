﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyWay_Quotation.Models
{
    public class Info
    {
        public string Addr { set; get; }
        public string Phone { set; get; }
        public string Uninum { set; get; }
        public string Logoname { set; get; }
    }
}
