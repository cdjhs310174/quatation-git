﻿using System;
using System.Collections.Generic;

namespace MyWay_Quotation.Models
{
    public partial class CusOrder
    {
        public int Unique { get; set; }
        public int Cusid { get; set; }
        public int? Uninum { get; set; }
        public DateTime? Trandate { get; set; }
        public string Quodate { get; set; }
        public DateTime? Valdate { get; set; }
    }
}
