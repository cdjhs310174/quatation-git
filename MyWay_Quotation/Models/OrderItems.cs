﻿using System;
using System.Collections.Generic;

namespace MyWay_Quotation.Models
{
    public partial class OrderItems
    {
        public string Cusid { get; set; }
        public int Discount { get; set; }
        public int Price { get; set; }
        public int Quan { get; set; }
        public string Itemsid { get; set; }
        public DateTime Createdate { get; set; }
        public DateTime Updatedate { get; set; }
    }
}
