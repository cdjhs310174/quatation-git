﻿using System;
using System.Collections.Generic;

namespace MyWay_Quotation.Models
{
    public partial class UserRemarks
    {
        public string Cusid { get; set; }
        public string Warranty { get; set; }
        public string Origin { get; set; }
        public string DeliveryDate { get; set; }
        public string ServiceDate { get; set; }
        public string Holding { get; set; }
    }
}
