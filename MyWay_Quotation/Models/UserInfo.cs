﻿using System;
using System.Collections.Generic;

namespace MyWay_Quotation.Models
{
    public partial class UserInfo
    {
        public string Userid { get; set; }
        public string Addr { get; set; }
        public string Phone { get; set; }
        public string Uninum { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
