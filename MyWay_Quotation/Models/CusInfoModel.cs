﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyWay_Quotation.Models
{

    public class CusInfoModel
    {

        public string cusname { set; get; }
        public string comname { set; get; }
        public string comaddr { set; get; }
        public string comphone { set; get; }

        public DateTime? comtrandate { set; get; }
        public DateTime comquonum { set; get; }
        public string comuninum { set; get; }
        public DateTime? comvaldate { set; get; }
    }
}
