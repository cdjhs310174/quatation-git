﻿using System;
using System.Collections.Generic;

namespace MyWay_Quotation.Models
{
    public partial class CusCompany
    {
        public int Cusid { get; set; }
        public string Name { get; set; }
        public string CoName { get; set; }
        public string CoAddress { get; set; }
        public string CoPhone { get; set; }
        public DateTime? Createdate { get; set; }
        public DateTime? Updatedate { get; set; }
        public int Unique { get; set; }
    }
}
