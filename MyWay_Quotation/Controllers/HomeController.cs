﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyWay_Quotation.Models;
using NuGet.Protocol.Core.Types;
using MyWay_Quotation.ViewModels;
using Newtonsoft.Json;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Microsoft.VisualBasic;
using Microsoft.AspNetCore.Razor.Language.CodeGeneration;

namespace MyWay_Quotation.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private MyWay_SystemContext ctx;/*從外面的 MyWay_SystemContext 宣告CTX*/
        public HomeController(ILogger<HomeController> logger, MyWay_SystemContext ctx)
        {
            _logger = logger;

            this.ctx = ctx;/*外面的 MyWay_SystemContext 的ctx =裡面的ctx*/
        }
        public IActionResult Index()
        {
            Debug.WriteLine("Home Index");/*DEBUG訊息*/
            UserInfo company = ctx.UserInfo.ToList().FirstOrDefault();/*CusCompany 宣告company是 MyWay_SystemContext宣告的ctx的第一項(清單式呈現)*/
            UserRemarks remarks = ctx.UserRemarks.ToList().FirstOrDefault();
            CusOrder cusorder = ctx.CusOrder.ToList().FirstOrDefault();
            CusCompany cuscompany = ctx.CusCompany.ToList().FirstOrDefault();


            IndexViewModel viewModel = new IndexViewModel();/*直接從viewmodel給值*/


            viewModel.CompanyAddr = company.Addr;
            viewModel.CompanyPhone = company.Phone;
            viewModel.CompanyUninum = company.Uninum;
            viewModel.CompanyEmail = company.Email;
            viewModel.CompanyName = company.Name;
            
            viewModel.Remarks = new User_remarks()
            {
                Origin = remarks.Origin,
                Warranty = remarks.Warranty,
                Delivery_date = remarks.DeliveryDate,
                Service_date = remarks.ServiceDate,
                Holding = remarks.Holding

            };

            viewModel.Cuscompany = new Cuscompany()
            {
                Name = cuscompany.Name,
                CoName = cuscompany.CoName,
                CoAddress = cuscompany.CoAddress,
                CoPhone = cuscompany.CoPhone
            };
            viewModel.Cusorder = new Cusorder()
            {
                Uninum = cusorder.Uninum.ToString(),
                Trandate = cusorder.Trandate,
                Quodate = cusorder.Quodate,
                Valdate = cusorder.Valdate
            };

            //viewModel.CostumNames = new List<string>() { "AA", "BB", "CC" };

            //Lambda
            //viewModel.CostumNames = ctx.CusCompany.Select(x => x.Name).ToList();

            //LINQ
            //var CusNamList =
            //    from a in ctx.CusCompany
            //    select a.Name;
            //viewModel.CostumNames = CusNamList.ToList();

            //viewModel.CostumList = ctx.CusCompany.Select(x => new CustomInfo()
            //{
            //    label = x.Name,
            //    value = x.Name,
            //    company = x
            //}).ToList();

            //viewModel.CostumList = ctx.CusCompany.Join(ctx.CusOrder,  //浪打語法   
            //                                   a => a.Cusid,
            //                                   b => b.Cusid,
            //                                   (a, b) => new CustomInfo {
            //                                       label = a.Name,
            //                                       value = a.Name,
            //                                       company = a,
            //                                       order = b

            //                                   }).ToList();


            var CustlistQuery =
                from a in ctx.CusCompany
                join b in ctx.CusOrder on a.Cusid equals b.Cusid
                select new CustomInfo()
                {
                    label = a.Name,
                    value = a.Name,
                    company = a,
                    order = b
                };

            viewModel.CostumList = CustlistQuery.ToList();


            /*--------------------------------------------------------------------------------*/

            viewModel.MywayProds = new List<MywayProduct>();/*陣列式宣告*/

            MywayProduct prod1 = new MywayProduct();/*給陣列裡面的物件值*/
            prod1.Name = "Fever Guard";
            //prod1.Value = "(1)軟體:人臉追蹤測溫軟體<br>(2)型號:FGA11-IR1<br>(3)電腦:簡易型電腦";

            viewModel.MywayProds.Add(prod1);
            viewModel.MywayProds.Add(new MywayProduct/*合併方法*/
            {
                Name = "My Face",
                Value = "My_Face",
            });
            viewModel.MywayProds.Add(new MywayProduct/*合併方法*/
            {
                Name = "LineBot",
                Value = "LineBot",
            });

            /*--------------------------------------------------------------------------------*/
            return View(viewModel);

            //return View();
            //Cus_company Cus = new Cus_company();
            //Cus.Name = "余正威";
            //Cus.Co_address = "桃園市中壢區明德路60五樓I室";
            //Cus.Co_phone = "0989539391";
            //Cus.Co_name = "麥威科技";

            //Info info = new Info();
            //info.Addr = "桃園市中壢區明德路60五樓I室";
            //info.Phone = "電話:0989539391";
            //info.Uninum = "統一編號:82782328";
            //info.Logoname = "麥威科技有限公司";
            //// Tuple<Cus_company, Info> tuple = new Tuple<, Cus_companyInfo>(Cus, info);
            //var TupleModel = new Tuple<Info, Cus_company>(info, Cus);

            //Console.WriteLine(TupleModel);
            //return View(TupleModel);
        }
        public IActionResult Getname()
        {
            string searchname = null;
            using (Models.MyWay_SystemContext db = new MyWay_SystemContext())
            {
                //LINQ
                var result = (from s in db.CusCompany
                              where s.Name == searchname
                              select s).ToList();
                return View("Index", result);
            }
        }
        //modal post JSON過來後接收 用insert into 或add() 兩個table 
        public IActionResult Insert(IndexViewModel model)
        {
            try
            {
                Models.MyWay_SystemContext db = new MyWay_SystemContext();

                CusCompany modal = new CusCompany();
                
                //db.Dispose();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return View();
        }
        [HttpPost]
        public IActionResult Check([FromBody]Login login) 
        {
            using (var log=new MyWay_SystemContext())
            {
                string passuser = null;
                var id = (from s in log.UserLogin
                          where s.Ac == passuser
                          select s).ToList();
                return Ok();
            }
        }

        //[HttpPost]
        //public IActionResult fffff([FromBody] CusInfoModel modalname) 
        //{
        //    //1. 接收使用者資料

        //    //2. 產生報價單編號

        //    //3. 寫入資料庫

        //    //4. 回傳結果

        //    return Ok(new { res = 1, msg = "放入剛剛產生的報價單編號" });
        //}


        [HttpPost]
        public IActionResult Recivedata([FromBody] CusInfoModel modalname)
        {    
            try
            {
                using (var ctx = new MyWay_SystemContext())
                {
                    int CusID = ctx.CusCompany.OrderByDescending(x => x.Cusid).Select(x => x.Cusid).FirstOrDefault();
                    CusID++;
                    int trandate ;
                    trandate = 0;
                    DateTime dt = DateTime.Now;
                    trandate = ctx.CusOrder.OrderBy(x => x.Trandate).Select(x => x.Trandate).Where(month => month.Value.Year == dt.Year && month.Value.Month == dt.Month).Count();
                    trandate++;
                    Debug.WriteLine(trandate);
                    var quoyear = modalname.comquonum.Year.ToString();
                    var quomonth = modalname.comquonum.Month.ToString();
                    var receivename = modalname.cusname;
                    

                    ctx.CusCompany.Add(new CusCompany
                    {
                        Name = modalname.cusname,
                        CoAddress = modalname.comaddr,
                        CoName = modalname.comname,
                        CoPhone = modalname.comphone,
                        Cusid = CusID,
                        Createdate=DateTime.Now,
                        Updatedate=DateTime.Now
                    });
                    if (dt.Month < 10) 
                    {
                        ctx.CusOrder.Add(new CusOrder
                        {
                            Cusid = CusID,
                            Trandate = modalname.comtrandate,
                            Quodate = dt.Year + "0" + dt.Month + "_" + trandate.ToString(),
                            Valdate = modalname.comvaldate,
                            Uninum = int.Parse(modalname.comuninum)
                        });
                    }
                    else if (dt.Month > 10)
                    {
                        ctx.CusOrder.Add(new CusOrder
                        {
                            Cusid = CusID,
                            Trandate = modalname.comtrandate,
                            Quodate = dt.Year + dt.Month + "_" + trandate.ToString(),
                            Valdate = modalname.comvaldate,
                            Uninum = int.Parse(modalname.comuninum)
                        });
                    }
                    

                    ctx.SaveChanges();
                }

                var aa = modalname;
                return Ok(new {res = 1, msg="OK" });

                //return View("Index","");
            }
            catch(Exception e)
            {

                return Ok(e.ToString());
            }

            //return View("Index", aa);
            //CusCompany temp = JsonConvert.DeserializeObject<CusCompany>(jdata);
        }
        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
